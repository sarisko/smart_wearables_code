#define POWER_PIN  D1
#define SIGNAL_PIN A2
# define vibration D3

int value = 0; // variable to store the sensor value

int ThermistorPin = A0;
int Vo;
int sensor_type=0;
float R1 = 10000;
float logR2, R2, T, Tc, Tf;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(POWER_PIN, OUTPUT);  
  digitalWrite(POWER_PIN, LOW); // turn the sensor OFF
  pinMode(vibration, OUTPUT); 
  digitalWrite(vibration, LOW); 

}

void loop() {
  // put your main code here, to run repeatedly:

  digitalWrite(POWER_PIN, HIGH);  // turn the sensor ON
  delay(10);                      // wait 10 milliseconds
  value = analogRead(SIGNAL_PIN); // read the analog value from sensor
  digitalWrite(POWER_PIN, LOW);   // turn the sensor OFF
  Serial.print("Sensor value: ");
  Serial.println(value);
  if (value > 3){
    digitalWrite(vibration, HIGH); 
  } else {
    digitalWrite(vibration, LOW); 
  }

  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  Tc = T - 273.15;
  Tf = (Tc * 9.0)/ 5.0 + 32.0; 
  
  Serial.print("Temperature from small sensor: "); 
  Serial.print(Tc);
  Serial.println(" C");   

  Serial.println(" ");
  Serial.println(" ");

  delay(1000);
  
}
